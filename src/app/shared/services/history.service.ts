import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
/**
 * @whatItDoes
 *  Loader state service to be shared throughout the application.
 *
 * @howToUse
 *  Implement using Dependency Injection and call the displayLoader method to set the state (true/false).
 */

@Injectable()
export class HistoryService {

  private outputHistory: string[] = [];
  private inputHistory: string[] = [];

  public getOutputHistory(): Observable<string[]> {
    return Observable.create(observer => {
        observer.next(this.outputHistory.slice(0, 10));
        observer.complete();
      }
    );
  }

  public getInputHistory(): Observable<string[]> {
    return Observable.create(observer => {
        observer.next(this.inputHistory.slice(0, 10));
        observer.complete();
      }
    );
  }

  public addInputHistory(val: string): void {
    this.inputHistory.unshift(val);
  }

  public addOutputHistory(val: string): void {
    this.outputHistory.unshift(val);
  }
}
