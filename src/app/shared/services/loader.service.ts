import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
/**
 * @whatItDoes
 *  Loader state service to be shared throughout the application.
 *
 * @howToUse
 *  Implement using Dependency Injection and call the displayLoader method to set the state (true/false).
 */

@Injectable()
export class LoaderService {
  public loaderStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  displayLoader(value: boolean) {
    this.loaderStatus.next(value);
  }
}
