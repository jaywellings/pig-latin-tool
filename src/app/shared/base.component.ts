/**
 * @whatItDoes
 * Base component to easily share common behaviours for all components.
 */
import {LoaderService} from './services/loader.service';
import {AfterViewInit, OnDestroy, OnInit} from '@angular/core';


export class BaseComponent implements AfterViewInit, OnDestroy, OnInit{

  constructor(private _loaderService: LoaderService){
  }

  public showLoader(): void {
    this._loaderService.displayLoader(true);
  }

  public hideLoader(): void {
    this._loaderService.displayLoader(false);
  }

  public ngOnDestroy() {}

  public ngAfterViewInit() {}

  public ngOnInit() {}
}
