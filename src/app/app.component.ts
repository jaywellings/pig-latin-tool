import {Component} from '@angular/core';
import {LoaderService} from './shared/services/loader.service';
import {BaseComponent} from './shared/base.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends BaseComponent{
  public loaderVisible: boolean = true;

  constructor( public loaderService: LoaderService) {
    super(loaderService);
  }

  public ngOnInit() {
    this.loaderService.loaderStatus.subscribe((val: boolean) => {
      this.loaderVisible = val;
    });
  }

}
