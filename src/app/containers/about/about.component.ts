import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../../shared/base.component';
import {LoaderService} from '../../shared/services/loader.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent extends BaseComponent {

  constructor(protected loaderService: LoaderService) {
    super(loaderService);
  }


}
