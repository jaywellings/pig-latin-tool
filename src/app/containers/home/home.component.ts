import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../shared/base.component';
import {LoaderService} from '../../shared/services/loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {animate, style, transition, trigger} from '@angular/animations';
import {HistoryService} from '../../shared/services/history.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('fadeAnimation', [
      transition(':enter', [
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ]),
      transition(':leave', [
        animate(500, style({opacity: 0}))
      ])
    ])
  ]
})
export class HomeComponent extends BaseComponent implements OnInit {

  public inputForm: FormGroup;
  public conversion: string = '';

  constructor(protected loaderService: LoaderService,
              private fb: FormBuilder,
              private historyService: HistoryService) {
    super(loaderService);
  }

  /**
   * Method is called onInit.
   *
   * Sets up the input form validation rules.
   *
   * @return  void
   */
  public ngOnInit(): void {
    this.inputForm = this.fb.group({
      userInput: ['', [Validators.required]]
    });
  }

  /**
   * Method is called on conversion click.
   *
   * Kicks off the process to calculate pig latin
   *
   * @return  void
   */
  public conversionSubmit(): void {

    const words = this.getWords(this.inputForm.controls['userInput'].value);

    this.processWords(words);
    this.conversion = this.processWords(words).join(' ');

    this.historyService.addInputHistory(this.inputForm.controls['userInput'].value);
    this.historyService.addOutputHistory(this.conversion);
  }

  /**
   * Method is called on when the user interacts with input
   *
   * @return  void
   */
  public inputChange(): void {
    this.conversion = '';
  }

  /**
   * splits input string into an array of strings.
   *
   * @return  string[]
   */
  public getWords(val: string): string[] {
    const words = val.split(/\b/);
    return words;
  }

  /**
   * Takes an array of words and returns an array of pig-latin words.
   * Only process word that a-z.
   *
   * @return  string[]
   */
  public processWords(words: string[]): string[] {
    const pigLatinWords = [];

    for (const word of words) {
      let pigLatinWord = word;

      if (word.match(/[a-z]/i)) {
        if (this.beginWithVowel(word)) {
          pigLatinWord = this.vowelReplacementPattern(word);
        } else {
          pigLatinWord = this.consonantsReplacementPattern(word);
        }
      }
      pigLatinWords.push(pigLatinWord);
    }

    return pigLatinWords;
  }

  /**
   * Takes a string and check if the word begins with a vowel
   *
   * @param String value to check
   *
   * @return if this value starts with constant
   */
  public beginWithVowel(word: string): boolean {
    const start = word.charAt(0).toLowerCase();
    return (start.match('(a|e|i|o|u)') !== null);
  }

  /**
   * Takes a character and check if it's a vowel
   *
   * @param String value to check
   *
   * @return if this value starts with constant
   */
  public isVowel(char: String): boolean {
    return (char.toLowerCase().match('(a|e|i|o|u)') !== null);
  }

  /**
   * Takes a string and follows this rule:
   * All letters before the initial vowel are placed at the end of the word sequence. Then, 'ay';
   *
   * @param String value to convert
   *
   * @return conversion of initial input string following this rule.
   */
  public consonantsReplacementPattern(word: String): string {
    let splitIndex = -1;
    let replacedWord;


    for (let i = 0; i < word.length; i++) {
      if (this.isVowel(word.charAt(i) )) {
        splitIndex = i;
        break;
      }
    }

    replacedWord = word.substring(splitIndex, word.length) + word.substring(0, splitIndex) + 'ay';

    return replacedWord;
  }

  /**
   * Takes a string and follows this rule:
   * For words that begin with vowel sounds, one just adds 'way' to the end;
   *
   * @param String value to convert
   *
   * @return conversion of initial input string following this rule.
   */
  public vowelReplacementPattern(word: String): string {
    return word + 'way';
  }

}
