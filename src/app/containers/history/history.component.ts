import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../../shared/services/history.service';
import {BaseComponent} from '../../shared/base.component';
import {LoaderService} from '../../shared/services/loader.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent extends BaseComponent implements OnInit {

  public output: string[];
  public input: string[];

  constructor(protected loaderService: LoaderService,
              private historyService: HistoryService) {
    super(loaderService);
  }

  public ngOnInit() {
    this.historyService.getOutputHistory()
      .subscribe((data) => {
      this.output = data;
    });

    this.historyService.getInputHistory()
      .subscribe((data) => {
        this.input = data;
      });
  }




}
