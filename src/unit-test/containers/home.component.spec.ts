import {HomeComponent} from '../../app/containers/home/home.component';
import {LoaderService} from '../../app/shared/services/loader.service';
import {FormBuilder} from '@angular/forms';
import {HistoryService} from '../../app/shared/services/history.service';

let app: HomeComponent;


describe('HomeComponent', () => {
  let ls: LoaderService;
  let fb: FormBuilder;
  let hs: HistoryService;

  beforeEach(function() {
    this.app = new HomeComponent(ls, fb, hs);
  });

  it('1 - Check the conversion string is initilesed at ""', function() {
    expect(this.app.conversion).toEqual('');
  });

  it('2 - Check that test sentance is split in an expected way', function() {
    const str = 'this, is. my test case (sometimes) it could BREAKS';
    expect(this.app.getWords(str)).toEqual(['this', ', ', 'is', '. ', 'my', ' ', 'test', ' ', 'case', ' (', 'sometimes', ') ', 'it', ' ', 'could', ' ', 'BREAKS']);
  });

  it('3 - Check consonants replace rule', function() {
    const str = 'pig';
    expect(this.app.consonantsReplacementPattern(str)).toEqual('igpay');
  });

  it('4 - Check consonants replace rule - uppercase', function() {
    const str = 'PIG';
    expect(this.app.consonantsReplacementPattern(str)).toEqual('IGPay');
  });

  it('5 - Check vowel replace rule', function() {
    const str = 'eat';
    expect(this.app.vowelReplacementPattern(str)).toEqual('eatway');
  });

  it('6 - Check vowel replace rule', function() {
    const str = 'EAT';
    expect(this.app.vowelReplacementPattern(str)).toEqual('EATway');
  });

  it('7 - is Vowel checker for value A', function() {
    const str = 'A';
    expect(this.app.isVowel(str)).toBeTruthy();
  });

  it('8 - is Vowel checker for value a', function() {
    const str = 'a';
    expect(this.app.isVowel(str)).toBeTruthy();
  });

  it('9 - is Vowel checker for value j', function() {
    const str = 'j';
    expect(this.app.isVowel(str)).toBeFalsy();
  });

  it('10 - is Vowel checker for value !', function() {
    const str = '!';
    expect(this.app.isVowel(str)).toBeFalsy();
  });

  it('11 - call the process method and check the value is expected', function() {
    const str = ['pig', ' ', 'eat', ' (', 'hello', ') ', '123'];
    expect(this.app.processWords(str).join('')).toEqual('igpay eatway (ellohay) 123');
  });

});
